const dotenv = require("dotenv");
const path = require("path");
const Joi = require("joi");

dotenv.config({ path: path.join(__dirname, "../.env") });

const envVarsSchema = Joi.object()
  .keys({
    NODE_ENV: Joi.string().valid("production", "development").default('development'),
    PORT: Joi.number().default(3100),
    JWT_SECRET: Joi.string().default('pruebatoken').description("JWT secret key"),
    JWT_ACCESS_EXPIRATION_MINUTES: Joi.number()
      .default(60)
      .description("minutes after which access tokens expire"),
    JWT_VERIFY_EMAIL_EXPIRATION_MINUTES: Joi.number()
      .default(10)
      .description("minutes after which verify email token expires"),
    JWT_RESET_PASSWORD_EXPIRATION_MINUTES: Joi.number()
      .default(10)
      .description("minutes after which reset password token expires"),
    BASE_URL_FILE: Joi.string()
      .uri()
      .default('https://echo-serv.tbxnet.com/')
      .description("url for file request"),
  })
  .unknown();

const { value: envVars, error } = envVarsSchema
  .prefs({ errors: { label: "key" } })
  .validate(process.env);

if (error) {
  console.log(`Config validation error:`, error.details);
  process.exit(1);
}

module.exports = {
  env: envVars.NODE_ENV,
  port: envVars.PORT,
  jwt: {
    secret: envVars.JWT_SECRET,
    access_expire: envVars.JWT_ACCESS_EXPIRATION_MINUTES,
    email_expire: envVars.JWT_VERIFY_EMAIL_EXPIRATION_MINUTES,
    password_expire: envVars.JWT_RESET_PASSWORD_EXPIRATION_MINUTES,
  },
  urls: {
    BASE_URL_FILE: envVars.BASE_URL_FILE,
  },
};
