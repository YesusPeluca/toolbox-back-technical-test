const express = require('express');
const xss = require('xss-clean');
const compression = require('compression');
const cors = require('cors');
const morgan = require('./config/morgan');

const app = express();
const groupRouter = require('./routes');

app.use(morgan.successHandler);
app.use(morgan.errorHandler);


// parse json request body
app.use(express.json());

// parse urlencoded request body
app.use(express.urlencoded({ extended: true }));

// sanitize request data
app.use(xss());

// gzip compression
app.use(compression());

// enable cors
app.use(cors());
app.options('*', cors());

groupRouter(app);



module.exports = app;


