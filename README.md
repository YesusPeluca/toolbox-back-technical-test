# Code Challenge toolbox - backend


## Instalacion con docker

El sistema esta isntalado docker por ende los comandos de docker para levantar:
```sh
docker-compose up 
```
si necesitas permisos de sistema en ubuntu se aria de esta manera
```sh
sudo docker-compose up 
```
## instalacion sin docker

El sistema esta isntalado docker por ende los comandos de docker para levantar:
```sh
npm install
```
para iniciar el sistema se inicia con dos comandos dev que levanta la api con nodemon y start que levanta la api con node  se dejan sus ejemplos
```sh
npm run dev
```
o

```sh
npm run start
```
## Rutas
El sistema se levanta a travez del puerto 3100 por defecto y esta proporcionado por varias rutas

| PATCH | METHOD | query | DESCRIPCCION | 
| ------ | ------ | ------ | ------ | 
| /file | GET | | te devuelve el nombre de todos los archivos
| /file/data | GET |  fileName | Devuelve los archivos que se encuentras en la api tambien puedes usar el query para poder filtrarlo |

## TEST

los test no se levantan con docker si no directamente en el sistema los compandos para ejecutarlos son

```sh
npm install
```
o

```sh
npm i
```
Despues que termine de hacer la isntalacion de dependencias debe utilizar el siguiente comando

```sh
npm run test
```

