FROM node:14-alpine as base

WORKDIR /app
COPY ./package*.json ./
EXPOSE 3100

FROM base as backend
ENV NODE_ENV=development
RUN npm install
COPY . /app

CMD ["node", "bin/www"]