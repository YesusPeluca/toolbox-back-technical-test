const httpStatus = require("http-status");
const ResponseError = require("../../helpers/ResponseError");
const Request = require("../../helpers/request");
const config = require("../../config/config");

const request = new Request(config.urls.BASE_URL_FILE, "aSuperSecretKey");

/**
 * Trae un listado de todos los archivos existente
 * @returns {Promise<void>}
 */
const getListFile = async () => {
  try {
    const listFile = await request.get("/v1/secret/files");
    return listFile.data;
  } catch (e) {
    throw new ResponseError(httpStatus.BAD_REQUEST, e);
  }
};

/**
 * Hace el llamado a cada uno de los archivos y espera que respondan
 * @param data // es un array de los archivos en el servidor para ser llamado
 * @returns {Array<JSON>}
 */
const getALlFile = async (data) => {
  try {
    const awaitRequest = data.map((e) => {
      return request.get(`/v1/secret/file/${e}`).catch("error");
    });

    // se usa allSetteled para que acepte todas las promesas y se filtrar en dos array
    const dataFull = await Promise.allSettled(awaitRequest)
      .then((values) => {
        const dataFull = [];
        for (let i = 0; i < values.length; i++) {
          const element = values[i];

          if (element.status == "rejected") {
            dataFull.push({
              file: data[i],
              lines: [],
            });
          } else {
            dataFull.push({
              file: data[i],
              lines: element.value.data.split("\n"),
            });
          }
        }
        return dataFull;
      })
      .catch((e) => {
        console.log("e :>> ", e);
      });

    return dataFull;
  } catch (e) {
    console.log("e :>> ", e);
    throw new ResponseError(httpStatus.BAD_REQUEST, e);
  }
};

module.exports = {
  getListFile,
  getALlFile,
};
