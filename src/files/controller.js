const httpStatus = require("http-status");
const errorHandler = require("../../middlewares/errors");
const ServiceFile = require("./services");
const { formatFile } = require("../../utils/formatFIle");

/**
 * Listado de data para los archivos
 * @param req
 * @param res
 * @returns {Promise<void>}
 */

const readFileController = async (req, res) => {
  try {
    const { fileName } = req.query;
    const nameFiles = await ServiceFile.getListFile();
    let files;
    if (fileName) {
      if (!nameFiles.files.includes(fileName)) {
        return errorHandler.errorHandler(
          {
            statusCode: httpStatus.FORBIDDEN,
            errorCode: 2000,
            message: "Este archivo no existe",
          },
          req,
          res
        );
      }
      files = [fileName];
    }

    const dataFull = await ServiceFile.getALlFile(files || nameFiles.files);
    const fileBeforeValidation = [];
    /* 
      * recorremos cada uno de los archivos recibidos por el servidor para empezare a formatearlos
      * e ignorar los campos que no cumplas las condiciones que se le indiquen
    */
    for (let i = 0; i < dataFull.length; i++) {
      const element = dataFull[i];
      fileBeforeValidation.push({
        file: element.file,
        lines: formatFile(element.lines),
      });
    }

    return res.status(httpStatus.OK).json({
      status: httpStatus.OK,
      data: fileBeforeValidation,
    });
  } catch (e) {
    errorHandler.errorHandler(
      { statusCode: e.statusCode, message: e },
      req,
      res
    );
  }
};

/**
 * Listado de nombres archivos
 * @param req
 * @param res
 * @returns {Promise<void>}
 */

const filesController = async (req, res) => {
  try {
    const nameFiles = await ServiceFile.getListFile();
    return res.status(httpStatus.OK).json({
      status: httpStatus.OK,
      data: {
        file: nameFiles.files,
      },
    });
  } catch (e) {
    console.log(e);
    errorHandler.errorHandler(
      { statusCode: e.statusCode, message: e },
      req,
      res
    );
  }
};

module.exports = {
  readFileController,
  filesController,
};
