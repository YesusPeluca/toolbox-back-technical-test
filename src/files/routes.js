const controllersFile = require('./controller');

/**
 * Router module Auth
 * @param routerGroup Grupo de rutas
 */
module.exports = (routerGroup) => {
  routerGroup.get('/', controllersFile.filesController);
  routerGroup.get('/data', controllersFile.readFileController);
};
