let chai = require("chai");
let chaiHttp = require("chai-http");
let server = require("../app");
let should = chai.should();

server.on("ready", function () {
  server.listen(3000, function () {
    console.log("app is ready");
  });
});

chai.use(chaiHttp);
describe("file", function () {
  it("Get name all files", function (done) {
    this.timeout(20000);
    chai
      .request(server)
      .get("/file")
      .end((err, res) => {
        res.should.have.status(200);
        res.body.data.file.should.be.a("array");
        done();
      });
  });
  it("Get  all data files", function (done) {
    this.timeout(20000);
    chai
      .request(server)
      .get("/file/data")
      .end((err, res) => {
        res.should.have.status(200);
        res.body.should.be.a("object");
        res.body.data.should.be.a("array");
        res.body.data.every((e) => {
          e.should.property("file");
          e.should.property("lines");
        });
        done();
      });
  });
  it("Get fetch data from an existing file", function (done) {
    this.timeout(20000);
    chai
      .request(server)
      .get("/file/data")
      .query({ fileName: "test9.csv" })
      .end((err, res) => {
        res.should.have.status(200);
        res.body.data.should.be.a("array");
        res.body.data.every((e) => {
          e.should.property("file");
          e.should.property("lines");
        });

        done();
      });
  });
  it("Get should fetch data from an non-existing file", function (done) {
    this.timeout(20000);
    chai
      .request(server)
      .get("/file/data")
      .query({ fileName: "test10.csv" })
      .end((err, res) => {
        res.should.have.status(403);
        res.body.should.have.property("errorCode").eql("E-2000");
        done();
      });
  });
});
