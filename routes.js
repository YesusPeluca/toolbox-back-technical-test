const Router = require('express-group-router');
const httpStatus = require('http-status');

const router = new Router();
const routerFile = require('./src/files/routes');



const ResponseError = require('./helpers/ResponseError');
const { errorConverter, errorHandler } = require('./middlewares/errors');

// ROUTERS
router.group('/file', [], (routerAPI) => routerFile(routerAPI));


const groupRoutes = router.init();

module.exports = (app) => {
  app.use(groupRoutes);

  // send back a 404 error for any unknown api request
  app.use((req, res, next) => {
    next(errorHandler(new ResponseError(httpStatus.NOT_FOUND, 'Not found'), req, res));
  });

  // convert error to ApiError, if needed
  app.use(errorConverter);

  // handle error
  app.use(errorHandler);
};
