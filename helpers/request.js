/* eslint-disable class-methods-use-this */
const { default: axios } = require("axios");

function setHeaders() {
  return {
    "Content-Type": "application/json",
    "Access-Control-Request-Origin": "*",
    "Access-Control-Request-Methods": "GET, POST, OPTIONS, PUT, DELETE",
  };
}

class Request {
  constructor(baseURL, token) {
    const request = axios.create({
      baseURL,
    });
    request.interceptors.request.use((config) =>
      this.authorizationInterceptor(config, token)
    );
    request.interceptors.response.use(this.handleSuccess, this.handleError);
    this.request = request;
  }

  authorizationInterceptor(config, token) {
    const newConfig = config;
    if (token) {
      newConfig.headers.authorization = `Bearer ${token}`;
    }
    return newConfig;
  }

  handleSuccess(response) {
    return response;
  }

  handleError(error) {
    //console.log(error)
    throw error.response;
  }

  get(path, payload) {
    return this.request.get(path, {
      params: payload,
    });
  }

  delete(path, payload) {
    return this.request.delete(path, payload);
  }

  post(path, payload) {
    return this.request.request({
      method: "POST",
      url: path,
      headers: setHeaders(),
      cache: "default",
      responseType: "json",
      data: payload,
    });
  }

  put(path, payload) {
    const resp = this.request.request({
      method: "PUT",
      url: path,
      headers: setHeaders(),
      responseType: "json",
      data: payload,
    });
    return resp;
  }
}

module.exports = Request;
