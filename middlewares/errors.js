const httpStatus = require('http-status');

const ResponseError = require('../helpers/ResponseError');


const errorConverter = (err, req, res, next) => {
  let error = err;

  if (!(error instanceof ResponseError)) {
    const { statusCode } = error;
    const message = error.message.toString() || httpStatus[statusCode];
    error = new ResponseError(statusCode, message, false);
  }

  next(error);
};

const errorHandler = (err, req, res, next) => {
  const { statusCode, message, errorCode } = err;
  const response = {
    code: statusCode === undefined ? httpStatus.BAD_GATEWAY : statusCode,
    errorCode: errorCode === undefined ? 1 : `E-${errorCode}`,

    message,
  };

  res.status(statusCode === undefined ? httpStatus.BAD_GATEWAY : statusCode).json(response);
};

module.exports = {
  errorConverter,
  errorHandler,
};
