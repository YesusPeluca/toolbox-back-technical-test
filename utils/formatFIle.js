const Joi = require("joi");

/**
 * Formateado del archivo
 * @param {array} data
 * @returns {Object}
 */

const formatFile = (data) => {
  const arrayJsonFile = [];

  if (!data.length) {
    return arrayJsonFile;
  }
  /*
   *la primera linea del del archivo son las cabezeras las dividimos por empaciado '\n' y despues por ','
   */
  const header = data[0].split("\n")[0].split(",");

  for (let i = 1; i <= data.length - 1; i++) {
    const element = data[i].split(",");
    const constructorObject = {};
    /*
      hacemos un for a las cabezeras 'header' para empezar a armar la estructura 
    */
    for (let e = 0; e <= header.length - 1; e++) {
      const elementHeader = header[e];
      constructorObject[elementHeader] = element[e];
    }
    /*
     * si el json armado no pasa la validacion se descarta automaticamente
     */
    if (validateFile(constructorObject)) {
      arrayJsonFile.push(constructorObject);
    }
  }

  return arrayJsonFile;
};

/**
 * Objeto que sera validado para agregar
 * @param {Object} data
 * @returns {Promise<void>}
 */
const validateFile = (data) => {
  const schema = Joi.object({
    file: Joi.string().required(),
    text: Joi.string().required(),
    number: Joi.number().required(),
    hex: Joi.string().required().min(32).max(32),
  });
  const { value, error } = schema.validate(data, {});
  return !error ? true : false;
};

module.exports = { formatFile, validateFile };
